/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.cmdintepreter.interfaces;

import org.dstorerdb.cmdintepreter.cmd.Command;

@FunctionalInterface
/**
 *
 * @author martin
 */
public interface InterpreterListener {
    public void exec(Command cmd);
}
