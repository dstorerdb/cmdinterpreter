/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.cmdintepreter.cmd;

/**
 *
 * @author martin
 */
public class BadSyntaxException extends Exception {

    /**
     * Creates a new instance of <code>BadSyntaxException</code> without detail
     * message.
     */
    public BadSyntaxException() {
    }

    /**
     * Constructs an instance of <code>BadSyntaxException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public BadSyntaxException(String msg) {
        super(msg);
    }
}
