/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.cmdintepreter.cmd;

import java.util.ArrayList;
import java.util.Arrays;

import org.dstorerdb.common.system.NumberID;

/**
 *
 * @author martin
 */
public class Command implements Interpretable{
    private String order;
    private String[] options;

    public Command(String order, String... options) {
        this.order = order;
        this.options = options;
    }

    public Command(String strCmd) throws BadSyntaxException {
        //countComillas(strCmd);
        buildCommand(strCmd);
    }
    
//    private void getCommand(String strCmd){
//        StringBuilder sbCmd = new StringBuilder();
//        ArrayList<String> listOptions = new ArrayList<>();
//        final char[] chars = strCmd.toCharArray();
//        final int charsLen  = chars.length;
//        
//        char c;
//        int nextIndex = 0;
//        
//        for (int i = 0; i < charsLen && (c = chars[i]) != ' '; i++, ++nextIndex)
//            sbCmd.append(c);
//        
//        order = sbCmd.toString();
//        sbCmd.delete(0, sbCmd.length());
//        boolean hasOptions = nextIndex < charsLen;
//        if (!hasOptions)
//            return;
//            
//        for (int i = nextIndex; i < charsLen; i++) {
//            
//        }
//        
//    }
    
    private void countQuotes(String strCommand) throws BadSyntaxException{
        int count = 0;
        final char[] chars = strCommand.toCharArray();
        for (int i = 0; i < chars.length; i++)
            if (chars[i] == '\'')
               count++;
        if (count > 0 && count % 2 != 0)
            throw new BadSyntaxException("Sintaxis del comando invalida");
    }
    
    // Se esta programando el tema de las comillas simples en el comando
    private void buildCommand(String strCommand) throws BadSyntaxException{
        final ArrayList<String> listOptions = new ArrayList<>();
        final String[] spaceSplit = strCommand.split(" ");
        order = spaceSplit[0];
        if (order.contains("'"))
            throw new BadSyntaxException("La orden no puede contener comillas");

        StringBuilder sbOptions = new StringBuilder();
        boolean quoteFind = false;
        int quoteIndex;
        String opt;

        for (int i = 1; i < spaceSplit.length; i++) {
            opt = spaceSplit[i];
            if (quoteFind) {
                quoteIndex = opt.indexOf("\"");
                sbOptions.append(' ');
                if (quoteIndex == -1)
                    sbOptions.append(opt);
                else {
                    sbOptions.append(opt, 0, quoteIndex+1);
                    quoteFind = false;
                    listOptions.add(sbOptions.toString());
                    sbOptions.delete(0, sbOptions.length());
                    if (quoteIndex < opt.length()-1)
                        listOptions.add(opt.substring(quoteIndex+1));
                }
            }
            else {
                quoteIndex = opt.indexOf("\"");
                quoteFind = quoteIndex != -1;
                if (quoteFind)
                    sbOptions.append(opt.substring(quoteIndex));
                else
                    listOptions.add(opt);
            }
        }
        options = new String[listOptions.size()];
        options = listOptions.toArray(options);
    }
    
    public boolean hasOptions(){
        return options != null;
    }
    
    public boolean isNumberOption(int index){
        try {
            Long.parseLong(getOptionAt(index));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean hasNumberID(String option){
        final char lastC = option.charAt(option.length()-1);
        return option.length() > 1 && 
                (lastC == NumberID.LONG ||
                lastC == NumberID.INT || 
                lastC == NumberID.SHORT || 
                lastC == NumberID.BYTE || 
                lastC == NumberID.FLOAT || 
                lastC == NumberID.DOUBLE);
    }
    
    public int getOptionAsNumber(int index){
        return isNumberOption(index) ? Integer.parseInt(getOptionAt(index)) : -1;
    }
    
    public int getOptionsCount(){
        return hasOptions() ? options.length : 0;
    }
    
    public String getOrder() {
        return order;
    }

    public String[] getOptions() {
        return options;
    }

    public String getOptionAt(int index){
        return options[index];
    }
    
    @Override
    public String toString(){
        StringBuilder sbCmd = new StringBuilder();
        sbCmd.append(order);
        
        final int optLen = options != null ? options.length : 0;
        for (int i = 0; i < optLen; i++)
            sbCmd.append(' ').append(options[i]);
        return sbCmd.toString();
    }
    
    /*public static void main(String[] args) throws BadSyntaxException {
        Command cmd = new Command("order 'opcion1 opcion2 opcion3 opcion4' opcion5");
        System.out.println(cmd.toString());
        System.out.println("Cantidad de opciones: "+cmd.getOptionsCount());
        for (int i = 0; i < cmd.getOptionsCount(); i++) {
            System.out.printf("Opcion %d: %s\n", i, cmd.getOptionAt(i));
        }
        
    }*/

    public static void main(String[] args) throws BadSyntaxException {
        Command cmd = new Command("add \"hola xd\" to list");
        System.out.println("Options: "+Arrays.toString(cmd.getOptions()));
        System.out.println(cmd.toString());
    }

}
