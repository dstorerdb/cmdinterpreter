/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.cmdintepreter.cmd;

/**
 *
 * @author martin
 */
public class CmdOption {
    public static final String ALL = "all";
    public static final String COUNT = "count";
    public static final String FROM = "from";
    public static final String IN = "in";
    public static final String INDEX = "index";
    public static final String LIST = "list";
    public static final String LISTS = "lists";
    public static final String NICK = "nick";
    public static final String PASSW = "passw";
    public static final String PORT = "port";
    public static final String PORTS = "ports";
    public static final String SIZEOF = "sizeof";
    public static final String TO = "to";
    public static final String TYPE = "type";
    public static final String TYPEOF = "typeof";
    public static final String USR = "usr";
    public static final String USRS = "usrs";
    public static final String WHERE = "where";
    public static final String EQUALS = "=";
    public static final String CONTAINS = "=%";

    
}
