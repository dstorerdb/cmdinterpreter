/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dstorerdb.cmdintepreter.cmd;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public class CmdOrder {
    public static final String ADD = "add";
    public static final String CLEAR = "clear";
    public static final String CLOSE = "close";
    public static final String CREATE = "create";
    public static final String DEL = "del";
    public static final String GET = "get";
    public static final String HELP = "help";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String RESTORE = "restore";
    public static final String SET = "set";

    public static final boolean isAvailableOrder(String order){
        Field[] fields = CmdOrder.class.getDeclaredFields();
        CmdOrder obj = new CmdOrder();
        
        for (int i = 0; i < fields.length; i++) {
            try {
                if (fields[i].get(obj).toString().equals(order))
                    return true;
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(CmdOrder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
        
        /*order = order.toLowerCase();
        return order.equals(ADD) || order.equals(CLEAR) || order.equals(DEL)
                || order.equals(GET) || order.equals(LOGIN) || order.equals(RESTORE)
                || order.equals(SET);*/
    }
    
}
